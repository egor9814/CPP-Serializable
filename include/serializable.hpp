//
// Created by egor9814 on 01.09.18.
//

#ifndef __egor9814__cpp_serializable_hpp__
#define __egor9814__cpp_serializable_hpp__

#include <vector>
#include <string>
#include <sstream>

namespace egor9814 {

    namespace detail {

        typedef unsigned char byte_t;
        typedef std::vector<byte_t> byte_array_t;

    }

    typedef detail::byte_array_t DataType;

    namespace detail {

        struct DataRW {
            unsigned long pos{0};

            void writeByte(byte_t value) {
                data.push_back(value);
                pos++;
            }

            void writeBytes(byte_t *value, unsigned long size) {
                for (auto i = 0ul; i < size; i++)
                    data.push_back(value[i]);
                pos += size;
            }

            byte_t readByte() {
                if (pos >= data.size())
                    return 0;
                return data[pos++];
            }

            unsigned long readBytes(byte_t *value, unsigned long size) {
                auto dataSize = data.size() - pos;
                auto count = dataSize < size ? dataSize : size;
                for (auto i = 0; i < count; i++)
                    value[i] = readByte();
                return count;
            }


            DataRW() = default;

            DataRW(const DataRW &d) = default;

            DataRW(DataRW &&d) noexcept : data(std::move(d.data)), pos(d.pos) {
                d.pos = 0;
            }

            ~DataRW() = default;

            DataRW &operator=(const DataRW &d) {
                if (this != &d) {
                    data = d.data;
                    pos = d.pos;
                }
                return *this;
            }

            DataRW &operator=(DataRW &&d) noexcept {
                if (this != &d) {
                    data = std::move(d.data);
                    pos = d.pos;

                    d.pos = 0;
                }
                return *this;
            }


        private:
            DataType data;
        };

    }

    struct Serializer;
    struct Deserializer;

    struct Serializable {

        virtual void serialize(Serializer &) = 0;

        template<typename T>
        struct Creator {
            virtual T createObject(Deserializer &) = 0;

            virtual T *createPointer(Deserializer &) = 0;

            /*virtual T *createObjectArray(Deserializer &) = 0;

            virtual T **createPointerArray(Deserializer &) = 0;*/
        };

        template<typename T>
        struct DefaultCreator : Creator<T> {
            T createObject(Deserializer &deserializer) override;

            T *createPointer(Deserializer &deserializer) override;
        };
    };


    struct Serializer {
        friend struct Deserializer;

        explicit Serializer(const Deserializer &);

        Serializer() = default;

        Serializer(const Serializer &s) = default;

        Serializer(Serializer &&s) noexcept : data(std::move(s.data)) {}

        ~Serializer() = default;

        Serializer &operator=(const Serializer &s) {
            if (this != &s) {
                data = s.data;
            }
            return *this;
        }

        Serializer &operator=(Serializer &&s) noexcept {
            if (this != &s) {
                data = std::move(s.data);
            }
            return *this;
        }


        template<typename NumberType>
        void serializeNumber(const NumberType &value) {
            union {
                NumberType number{0};
                detail::byte_t buf[sizeof(NumberType)];
            } u;
            u.number = value;
            data.writeBytes(u.buf, sizeof(u.buf));
        }

        void serializeString(const std::string &value) {
            for (auto &i : value) {
                data.writeByte(static_cast<detail::byte_t>(i));
            }
            data.writeByte(0);
        }

        void serializeWString(const std::wstring &value) {
            union {
                wchar_t c{0};
                detail::byte_t buf[2];
            } u;
            for (auto &i : value) {
                u.c = i;
                data.writeBytes(u.buf, 2);
            }
            data.writeByte(0);
            data.writeByte(0);
        }


        void serializeBool(const bool &value) {
            data.writeByte(static_cast<detail::byte_t>(value ? 1 : 0));
        }

        template<typename Container>
        void serializeContainer(const Container &container) {
            serializeNumber(container.size());
            for (auto &i : container) {
                *this << i;
            }
        }

        void serialize(const Serializable &value) {
            const_cast<Serializable &>(value).serialize(*this);
        }

    private:
        detail::DataRW data;
    };


    inline Serializer &operator<<(Serializer &out, char value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, unsigned char value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, short value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, unsigned short value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, int value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, unsigned int value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, long value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, unsigned long value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, float value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, double value) {
        out.serializeNumber(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, bool value) {
        out.serializeBool(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, const std::string &value) {
        out.serializeString(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, const std::wstring &value) {
        out.serializeWString(value);
        return out;
    }

    inline Serializer &operator<<(Serializer &out, const Serializable &value) {
        out.serialize(value);
        return out;
    }


    struct Deserializer {
        friend struct Serializer;

        explicit Deserializer(const Serializer &);

        Deserializer() = default;

        Deserializer(const Deserializer &) = default;

        Deserializer(Deserializer &&d) noexcept : data(std::move(d.data)) {}

        ~Deserializer() = default;

        Deserializer &operator=(const Deserializer &d) {
            if (this != &d) {
                data = d.data;
            }
            return *this;
        }

        Deserializer &operator=(Deserializer &&d) noexcept {
            if (this != &d) {
                data = std::move(d.data);
            }
            return *this;
        }


        template<typename NumberType>
        NumberType deserializeNumber() {
            union {
                NumberType value{0};
                detail::byte_t buf[sizeof(NumberType)];
            } u;
            data.readBytes(u.buf, sizeof(u.buf));
            return std::move(u.value);
        }

        bool deserializeBool() {
            return data.readByte() == 1;
        }

        std::string deserializeString() {
            std::stringstream ss;
            for (auto i = data.readByte(); i != 0; i = data.readByte())
                ss << static_cast<char>(i);
            return std::move(ss.str());
        }

        std::wstring deserializeWString() {
            std::wstringstream ss;
            union {
                wchar_t c{0};
                detail::byte_t buf[2];
            } u;
            data.readBytes(u.buf, 2);
            while (u.c != 0) {
                ss << u.c;
                data.readBytes(u.buf, 2);
            }
            return std::move(ss.str());
        }

        template<typename Container>
        void deserializeContainer(Container &container) {
            auto size = deserializeNumber<unsigned long>();
            for (auto i = 0; i < size; i++) {
                typename Container::value_type value;
                *this >> value;
                container.push_back(std::move(value));
            }
        }

        template<typename T>
        T deserialize() {
            return std::move(T::CREATOR().createObject(*this));
        }

        template <typename T>
        T* deserializeAsPointer() {
            return T::CREATOR().createPointer(*this);
        }

    private:
        detail::DataRW data;
    };


    template<typename T>
    T Serializable::DefaultCreator<T>::createObject(egor9814::Deserializer &deserializer) {
        return std::move(T(deserializer));
    }

    template<typename T>
    T *Serializable::DefaultCreator<T>::createPointer(Deserializer &deserializer) {
        return new T(deserializer);
    }


    inline Deserializer &operator>>(Deserializer &in, char &value) {
        value = in.deserializeNumber<char>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, unsigned char &value) {
        value = in.deserializeNumber<unsigned char>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, short &value) {
        value = in.deserializeNumber<short>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, unsigned short &value) {
        value = in.deserializeNumber<unsigned short>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, int &value) {
        value = in.deserializeNumber<int>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, unsigned int &value) {
        value = in.deserializeNumber<unsigned int>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, long &value) {
        value = in.deserializeNumber<long>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, unsigned long &value) {
        value = in.deserializeNumber<unsigned long>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, float &value) {
        value = in.deserializeNumber<float>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, double &value) {
        value = in.deserializeNumber<double>();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, bool &value) {
        value = in.deserializeBool();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, std::string &value) {
        value = in.deserializeString();
        return in;
    }

    inline Deserializer &operator>>(Deserializer &in, std::wstring &value) {
        value = in.deserializeWString();
        return in;
    }

    template<typename T>
    inline Deserializer &operator>>(Deserializer &in, T &value) {
        value = std::move(in.deserialize<T>());
        return in;
    }


    Serializer::Serializer(const egor9814::Deserializer &d) : data(d.data) {}

    Deserializer::Deserializer(const egor9814::Serializer &s) : data(s.data) {
        data.pos = 0;
    }
}

#endif //__egor9814__cpp_serializable_hpp__
