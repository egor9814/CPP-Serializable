#include <iostream>
#include <serializable.hpp>

using namespace egor9814;

struct MySerial : Serializable {

    struct MyCreator : Serializable::DefaultCreator<MySerial> {
    };

    static MyCreator &CREATOR() {
        static MyCreator creator;
        return creator;
    }


    int a{0};
    float b{0};
    std::string c{};

    void serialize(Serializer &serializer) override {
        serializer << a << b << c;
    }

    MySerial() = default;

    explicit MySerial(Deserializer &in) {
        in >> a >> b >> c;
    }

    MySerial(const MySerial &) = default;

    MySerial(MySerial &&s) noexcept : a(s.a), b(s.b), c(std::move(s.c)) {
        s.a = 0;
        s.b = 0;
    }

    ~MySerial() = default;

    MySerial &operator=(const MySerial &s) {
        if (this != &s) {
            a = s.a;
            b = s.b;
            c = s.c;
        }
        return *this;
    }

    MySerial &operator=(MySerial &&s) noexcept {
        if (this != &s) {
            a = s.a;
            b = s.b;
            c = std::move(s.c);

            s.a = 0;
            s.b = 0;
        }
        return *this;
    }


    bool operator==(const MySerial &other) const {
        return a == other.a && b == other.b && c == other.c;
    }
};

int main() {
    std::cout << "Hello, World!" << std::endl;
    MySerial serial;
    serial.a = 43;
    serial.b = INT32_MAX;
    serial.c = "Hello";
    Serializer s;
    s << serial;
    Deserializer d(s);
    auto dSerial = d.deserializeAsPointer<MySerial>();
    std::cout << "equals: " << std::boolalpha << (serial == *dSerial) << std::endl;
    return 0;
}